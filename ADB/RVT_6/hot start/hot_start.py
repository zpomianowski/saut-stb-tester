# -*- coding: utf-8 -*-
import datetime
import time
import json
from COM import *
from mFi import *
from WKL64 import *
from ADBlib import *
from pyBMD import *
'''
FRAME = {"valid": 0, "invalid": 0}


def callback_frame(width, height, bytes_in_row, pixel_format, buff_p):
    return True


def callback_start():
    return True


def callback_stop(infalidnb, validnb):
    print "\tInvalid: %s, Valid: %s" % (infalidnb, validnb)
    FRAME["valid"] = validnb
    FRAME["invalid"] = infalidnb
    return True
'''


class VIDEO(Thread):
    def __init__(self, bmd=None, frames=10):
        Thread.__init__(self)
        self.bmd = None
        self.work_flag = True
        self.frames = frames
        self.valid = 0
        self.invalid = 0

    def callback_frame(self, width, height, bytes_in_row, pixel_format, buff_p):
        return True

    def callback_start(self):
        return True

    def callback_stop(self, infalidnb, validnb):
        self.valid += int(validnb)
        self.invalid += int(infalidnb)
        return True

    def run(self):
        self.bmd = BMD(0)
        self.bmd.setStartLoopCallback(self.callback_start)
        self.bmd.setStopLoopCallback(self.callback_stop)
        self.bmd.setFrameArrivedCallback(self.callback_frame)
        while self.work_flag:
            self.bmd.startLoop(self.frames)
            time.sleep(1)

    def reset_COUNTER(self):
        self.valid = 0
        self.invalid = 0

    def getInvalidFrames(self):
        return self.bmd.getInvalidFrames()

    def getValidFrames(self):
        return self.bmd.getValidFrames()

    def resetCOUNTERs(self):
        self.bmd.resetCOUNTERs()

    def get_COUNTER(self):
        return [self.invalid, self.valid]

    def stop(self):
        self.work_flag = False
        if self.bmd:
            self.bmd.stopLoop()
            self.bmd.delete()


if __name__ == "__main__":
    Ton = 10 * 60
    Toff = 5 * 60
    USB_STB = "/dev/ttyUSB0"
    ETH_MFI = "192.168.0.2"
    USB_CHAMBER = "/dev/ttyUSB1"
    MFI_SOCKET = 4
    filename = "hot_start_logs.log"
    frame_threshold = 0.99
    #  2h -5 st

    mFi = mFi(ETH_MFI, "ubnt", "ubnt")

    chamber = WKL64(19200, USB_CHAMBER, bus=1)
    chamber.start()
    time.sleep(2)
    chamber.capture_logs()
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(2*60*60)
    '''
    BMD_id = 0
    bmd = BMD(BMD_id)
    bmd.setStartLoopCallback(callback_start)
    bmd.setStopLoopCallback(callback_stop)
    bmd.setFrameArrivedCallback(callback_frame)
    '''
    video = VIDEO(frames=100)
    video.start()
    time.sleep(1)

    stb = None
    ErrorListStart = []
    ErrorListEnd = []
    CYCLES = 0
    ERRORS = 0
    START_TIMESTAMP = time.time()
    SET = False
    while True:
        if (time.time() - START_TIMESTAMP) >= 26*60*60:  # 24h
            break
        if ((time.time() - START_TIMESTAMP) >= 24*60*60) and (not SET):  # 24h
            chamber.set_values(temperature=25, oStart=True)
            print "chamber.set_values(temperature=25, oStart=True)"
            SET = True
        try:
            Decklink_OK = True
            TEST_SUCCESS = True
            print "TEST %s %s" % (CYCLES, datetime.datetime.now())
            with open(filename, "a") as myfile:
                myfile.write("\nTEST %s %s\n" % (CYCLES, datetime.datetime.now()))

            mFi.switchON(MFI_SOCKET)
            stb = COM(115200, USB_STB, 0, "com_log.log")
            stb.start()
            time.sleep(30)
            stb.send("transponder 0 802000 6875 q256")
            time.sleep(1)
            stb.send("avp_pids_set 0x101 0x100 0x100")   # avp_pids_set 0x204 0x201 0x201   257
            time.sleep(1)
            stb.send("video_decoder 3")
            time.sleep(1)
            stb.send("audio_decoder 2")

            video.reset_COUNTER()

            '''
            try:
                response = urllib2.urlopen('http://192.168.0.6:6446/Monitor/MonitorService.svc/Racks/local/1/reports/1/alarms', timeout=3)
                ErrorListStart = json.load(response)
            except:
                ErrorListStart = None
                with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())
            '''
            # ######################## TATS 0 ########################
            if not check_tats(stb, filename):
                TEST_SUCCESS = False
            # ######################## SET DHCP ######################
            if not setDHCPandPING(stb, filename):
                TEST_SUCCESS = False
            # ######################## DECKLINK ######################
            '''
            bmd.startLoop(100)
            if FRAME["invalid"] == 1:
                FRAME["invalid"] = FRAME["invalid"]-1
                FRAME["valid"] += 1
            with open(filename, "a") as myfile:
                myfile.write("\n\n\tValid: frames: %d" % (FRAME["valid"]))
                myfile.write("\n\tInvalid: frames: %d" % (FRAME["invalid"]))
                try:
                    myfile.write("\n\tVALID/ALL: %s" % float(((FRAME["valid"]+1)/(FRAME["valid"]+FRAME["invalid"]))))
                except:
                    pass

            try:
                if float(((FRAME["valid"])/(FRAME["valid"]+FRAME["invalid"]))) >= frame_threshold:
                    Decklink_OK = True
                else:
                    Decklink_OK = False
                    TEST_SUCCESS = False
            except:
                with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())
            '''

            time.sleep(Ton-53)

            print "\tVIDEO:"
            print "\t  valid:", video.valid
            print "\t  invalid:", video.invalid
            with open(filename, "a") as myfile:
                myfile.write("\n\tVIDEO:")
                myfile.write("\n\t  valid: %d" % video.valid)
                myfile.write("\n\t  invalid: %d" % video.invalid)
                try:
                    if (float(video.valid)/float(video.valid+video.invalid)) >= frame_threshold:
                        myfile.write("\n\tVIDEO - OK")
                        print "\tVIDEO - OK"
                    else:
                        TEST_SUCCESS = False
                        myfile.write("\n\tVIDEO - ERROR")
                        print "\tVIDEO - ERROR"
                except:
                    with open("script_error.log", "a") as myfile:
                        myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                        myfile.write("\n%s\n" % traceback.format_exc())

            time.sleep(4)

            '''
            try:
                response = urllib2.urlopen('http://192.168.0.6:6446/Monitor/MonitorService.svc/Racks/local/1/reports/1/alarms', timeout=3)
                ErrorListEnd = json.load(response)
            except:
                ErrorListEnd = None
                with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())
            '''
        except:
            with open("script_error.log", "a") as myfile:
                myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                myfile.write("\n%s\n" % traceback.format_exc())
        finally:
            try:
                if stb:
                    stb.stop()
                #rrResult = RRsystem(ErrorListStart, ErrorListEnd, filename)
                #print "rrResult", rrResult
                '''
                if (rrResult is True) and (Decklink_OK is True):
                    print "\tVIDEO - OK"
                    with open(filename, "a") as myfile:
                        myfile.write("\n\tVIDEO - OK")
                '''
                '''
                if Decklink_OK is True:
                    print "\tVIDEO - OK"
                    with open(filename, "a") as myfile:
                        myfile.write("\n\tVIDEO - OK")
                else:
                    TEST_SUCCESS = False
                    print "\tVIDEO - ERROR"
                    with open(filename, "a") as myfile:
                        myfile.write("\n\tVIDEO - ERROR")
                '''
                if not TEST_SUCCESS:
                    ERRORS += 1
                CYCLES += 1
                mFi.switchOFF(MFI_SOCKET)
                time.sleep(Toff)
            except:
                mFi.switchOFF(MFI_SOCKET)
                time.sleep(Toff)

    with open(filename, "a") as myfile:
        myfile.write("\n\n---------------------------------------\n\n")
        myfile.write("Cycle: %s\n" % CYCLES)
        myfile.write("Errors: %s" % ERRORS)
    chamber.stop_capturing_logs()
    chamber.set_values(temperature=25, oStart=False)
    chamber.stop()
