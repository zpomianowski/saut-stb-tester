# -*- coding: utf-8 -*-

import datetime
from COM import *
from mFi import *
from pyBMD import *
from WKL64 import *
import os
from sys import platform as _platform
import sys
import time
import re
import traceback
import urllib2
import json


FRAME = {"valid": 0, "invalid": 0}


def ping(hostname):
    if _platform == "linux" or _platform == "linux2":
        return os.system("ping -c 4 " + hostname)
    elif _platform == "win32":
        return os.system("ping " + hostname)


def callback_frame(width, height, bytes_in_row, pixel_format, buff_p):
    #print "dev_id: %d, w: %d, h: %d, inrow: %d" % (
        #    BMD_id, width, height, bytes_in_row)
    return True


def callback_start():
    #print "\tStart capturing frames"
    return True


def callback_stop(infalidnb, validnb):
    print "\tInvalid: %s, Valid: %s" % (infalidnb, validnb)
    FRAME["valid"] = validnb
    FRAME["invalid"] = infalidnb
    return True


def check_tats(stb, filename):
    stb.send("tats 0")
    time.sleep(4)
    response = stb.get_response()
    if "SPECTRUM" in response:
        print "\tTATS - OK"
        with open(filename, "a") as myfile:
            tmp = '\n\t  '.join(response.split('\n')[1:])
            tmp = tmp.replace('\r', '')
            myfile.write("\t%s" % tmp)
            myfile.write("\n\tTATS 0 - OK")
        return True
    else:
        TEST_SUCCESS = False
        print "\tTATS - ERROR"
        with open(filename, "a") as myfile:
            tmp = '\t  '.join(response.split('\n')[1:])
            myfile.write("\t%s" % tmp)
            myfile.write("\n\tTATS 0 - ERROR")
        return False


def setDHCPandPING(stb, logfile):
    stb.send("netdhcp 2 1")
    time.sleep(10)
    stb.send("netstatus 2")
    time.sleep(2)
    response = stb.get_response()
    with open(logfile, "a") as myfile:
        myfile.write("\n\n\tNETSTATUS 2:\n")
        tmp = '\n\t  '.join(response.split('\n')[1:])
        myfile.write("\t%s" % tmp)
    if "boot.prot" in response:
        with open(logfile, "a") as myfile:
            myfile.write("\n\tNETSTATUS 2 - OK")
    else:
        with open(logfile, "a") as myfile:
            myfile.write("\n\tNETSTATUS 2 - ERROR")

    hostname = []
    try:
        hostname = re.findall(r'ip:\s+(192.168.\d+.\d+)', response)
    except:
        print "ERRR - hostname"
        hostname = ["192.168.1.100"]
    # ######################## PING ###################### 4s
    if hostname[0] != "192.168.10.100":
        response = ping(hostname[0])
        if response == 0:
            print "\tPING", hostname[0], " - OK"
            with open(logfile, "a") as myfile:
                myfile.write("\n\tPING" + hostname[0] + " - OK")
            return True
        else:
            print "\tPING", hostname[0], " ERROR"
            with open(logfile, "a") as myfile:
                myfile.write("\n\tPING - ERROR")
            return False
    else:
        with open(logfile, "a") as myfile:
            myfile.write("\n\tPING - ERROR - not working dhcp")
            print "\tPING - ERROR - not working dhcp"
        return False

if __name__ == "__main__":
    USB_STB = "/dev/ttyUSB1"
    ETH_MFI = "192.168.0.2"
    MFI_SOCKET = 4
    USB_CHAMBER = "/dev/ttyUSB2"
    frame_threshold = 0.99

    Tcycle = 2*60

    chamber = WKL64(19200, "/dev/ttyUSB2", bus=1)
    chamber.start()

    TESTS = [{
        "subtestcase": [
             {"duration": 60, "temperature_gradient": 0.5,  # 60 minut
              "temperature": -5, "humidity": 50, "hStart": False},
             {"duration": 16*60, "temperature_gradient": 0,  # 16*60
              "temperature": -5, "humidity": 50, "hStart": False},
             {"duration": 60, "temperature_gradient": 0.5,  # 60
              "temperature": 45, "humidity": 50, "hStart": False}],
        "logfile": "test_4.1.1.log",
        "measurefile": "measure_4.1.1.log"},
        {
        "subtestcase": [
            {"duration": 40, "temperature_gradient": 0.5,
             "temperature": 45, "humidity": 50, "hStart": False},
            {"duration": 16*60, "temperature_gradient": 0,
             "temperature": 45, "humidity": 50, "hStart": False},
            {"duration": 40, "temperature_gradient": 0.5,
             "temperature": 25, "humidity": 50, "hStart": False}],  # minuty
        "logfile": "test_4.1.2.log",
        "measurefile": "measure_4.1.2.log"},
        {
        "subtestcase": [
            {"duration": 3*60, "temperature_gradient": 0,
             "temperature": 25, "humidity": 50, "hStart": False},
            {"duration": 40, "temperature_gradient": 0.5,
             "temperature": 45, "humidity": 50, "hStart": False},
            {"duration": 3*60, "temperature_gradient": 0,
             "temperature": 45, "humidity": 50, "hStart": False},
            {"duration": 40, "temperature_gradient": 0.5,
             "temperature": 25, "humidity": 50, "hStart": False}],  # minuty
        "logfile": "test_4.1.3.log",
        "measurefile": "measure_4.1.3.log"},
        {
        "subtestcase": [
            {"duration": 60, "temperature_gradient": 0,
             "temperature": 25, "humidity": 50, "hStart": True},
            {"duration": 10, "temperature_gradient": 0.5,
             "temperature": 30, "humidity": 95, "hStart": True},
            {"duration": 96*60, "temperature_gradient": 0,
             "temperature": 30, "humidity": 95, "hStart": True},
            {"duration": 10, "temperature_gradient": 0.5,
             "temperature": 25, "humidity": 50, "hStart": True},
            {"duration": 12*60, "temperature_gradient": 0,
             "temperature": 25, "humidity": 50, "hStart": True}],  # minuty
        "logfile": "test_4.1.4.log",
        "measurefile": "measure_4.1.4.log"}
        ]


    mFi = mFi(ETH_MFI, "ubnt", "ubnt")
    mFi.switchON(MFI_SOCKET)
    time.sleep(30)

    stb = COM(115200, USB_STB, 0, "com_log.log")
    stb.start()
    stb.send("transponder 0 802000 6875 q256")
    time.sleep(1)
    stb.send("avp_pids_set 0x101 0x100 0x100")   # avp_pids_set 0x204 0x201 0x201   257
    time.sleep(1)
    stb.send("video_decoder 3")
    time.sleep(1)
    stb.send("audio_decoder 2")
    stb.stop()
    stb = None

    print "START", datetime.datetime.now()

    BMD_id = 0
    bmd = BMD(BMD_id)
    bmd.setStartLoopCallback(callback_start)
    bmd.setStopLoopCallback(callback_stop)
    bmd.setFrameArrivedCallback(callback_frame)

    for test in TESTS:
        CYCLES = 0
        ERRORS = 0
        with open(test["logfile"], "a") as myfile:
            myfile.write("START TEST %s\n" % datetime.datetime.now())
        chamber.stop_capturing_logs()
        time.sleep(1)
        chamber.log_file = test["measurefile"]
        chamber.capture_logs()
        for subtest in test["subtestcase"]:
            START_TIMESTAMP = time.time()
            with open(test["logfile"], "a") as myfile:
                myfile.write("\nSTART SUBTEST %s" % datetime.datetime.now())
                myfile.write("\n\tDuration: %smin" % (subtest["duration"]))
                myfile.write("\n\Gradient: %sst/min" % (subtest["temperature_gradient"]))
                myfile.write("\n\tTemperature: %sst\n" % (subtest["temperature"]))
            PREV_TEST = True
            TEST_SUCCESS = True
            Decklink_OK = True
            if subtest["temperature_gradient"] > 0:
                chamber.set_gradients(heating=subtest["temperature_gradient"],
                                      cooling=subtest["temperature_gradient"],
                                      humidify=5, dehumidify=5)
                chamber.set_values_gradient(temperature=subtest["temperature"],
                                            humidity=subtest["humidity"],
                                            oStart=True,
                                            oHumidity=subtest["hStart"])
            else:
                chamber.set_values(temperature=subtest["temperature"],
                                   humidity=subtest["humidity"],
                                   oStart=True,
                                   oHumidity=subtest["hStart"])
            while True:
                if ((time.time() - START_TIMESTAMP) >= (subtest["duration"]*60)):  # *60
                    break
                try:
                    TEST_SUCCESS = True
                    Decklink_OK = True


                    print "TEST %s %s" % (CYCLES, datetime.datetime.now())
                    with open(test["logfile"], "a") as myfile:
                        myfile.write("\nTEST %s %s\n" % (CYCLES, datetime.datetime.now()))

                    stb = COM(115200, USB_STB, 0, "com_log.log")
                    stb.start()
                    # ######################## TATS 0 ########################
                    if not check_tats(stb, test["logfile"]):
                        TEST_SUCCESS = False
                    # ######################## SET DHCP ######################
                    if not setDHCPandPING(stb, test["logfile"]):
                        TEST_SUCCESS = False
                    # ######################## DECKLINK ######################

                    bmd.startLoop(100)
                    if FRAME["invalid"] == 1:
                        FRAME["invalid"] = FRAME["invalid"]-1
                        FRAME["valid"] += 1
                    with open(test["logfile"], "a") as myfile:
                        myfile.write("\n\n\tValid: frames: %d" % (FRAME["valid"]))
                        myfile.write("\n\tInvalid: frames: %d" % (FRAME["invalid"]))
                        myfile.write("\n\tVALID/ALL: %s" % float(((FRAME["valid"]+1)/(FRAME["valid"]+FRAME["invalid"]))))

                    try:
                        if float(((FRAME["valid"])/(FRAME["valid"]+FRAME["invalid"]))) >= frame_threshold:
                            Decklink_OK = True
                        else:
                            Decklink_OK = False
                            TEST_SUCCESS = False
                    except:
                        pass

                    time.sleep(Tcycle-24)

                except:
                    #print "SCRIPT ERROR", datetime.datetime.now()
                    with open("script_error.log", "a") as myfile:
                        myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                        myfile.write("\n%s\n" % traceback.format_exc())
                finally:
                    try:

                        if not Decklink_OK:
                            TEST_SUCCESS = False
                            with open(test["logfile"], "a") as myfile:
                                myfile.write("\n\tVIDEO - ERROR")
                        else:
                            with open(test["logfile"], "a") as myfile:
                                myfile.write("\n\tVIDEO - OK")
                        if stb:
                            stb.stop()
                        if not TEST_SUCCESS:
                            ERRORS += 1
                        if not TEST_SUCCESS and not PREV_TEST:
                            mFi.switchOFF(MFI_SOCKET)
                            time.sleep(2)
                            mFi.switchON(MFI_SOCKET)
                            time.sleep(30)
                            stb = COM(115200, USB_STB, 0, "com_log.log")
                            stb.start()
                            stb.send("transponder 0 802000 6875 q256")
                            time.sleep(1)
                            stb.send("avp_pids_set 0x101 0x100 0x100")   # avp_pids_set 0x204 0x201 0x201   257
                            time.sleep(1)
                            stb.send("video_decoder 3")
                            time.sleep(1)
                            stb.send("audio_decoder 2")
                            stb.stop()
                            stb = None

                        PREV_TEST = TEST_SUCCESS
                        CYCLES += 1
                    except:
                        print "a", traceback.format_exc()

        with open(test["logfile"], "a") as myfile:
            myfile.write("\n\n---------------------------------------\n\n")
            myfile.write("Cycle: %s\n" % CYCLES)
            myfile.write("Errors: %s" % ERRORS)

    print "END", datetime.datetime.now()
    if stb:
        stb.stop()
        stb = None
    chamber.stop_capturing_logs()
    chamber.stop()

#[{"Guid":"e82dc5e8-3e77-4fe5-a08b-5977634d96cf","Token":"01a699af-014a-4cc8-b5a6-d3393d14f709","Create":"\/Date(1440149445808+0200)\/","Mode":"Freeze","State":"Begin","Message":"Zamrozenie obrazu: rozpoczecie"}][{"Guid":"e82dc5e8-3e77-4fe5-a08b-5977634d96cf","Token":"01a699af-014a-4cc8-b5a6-d3393d14f709","Create":"\/Date(1440149445808+0200)\/","Mode":"Freeze","State":"Begin","Message":"Zamrozenie obrazu: rozpoczezcie"}]
