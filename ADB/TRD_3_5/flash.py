# -*- coding: utf-8 -*-
u'''
Komunikacja RS232
'''
import serial
import Queue
from threading import Thread
import threading
import sys
import traceback
import datetime
import re
from WKL64 import *
from mFi import *


class COM():
    def __init__(self, baudrate=115200, com='/dev/ttyUSB0', timeout=1):
        self.serial = serial.Serial()
        self.serial.baudrate = baudrate
        self.serial.port = com
        serial.timeout = timeout
        self._work_flag = True

        self.response = ""
        self.log_flag = False
        self.counter = 0
        self.qcmd = Queue.Queue()

        self.chamber = WKL64(19200, "/dev/ttyUSB0", bus=1)
        time.sleep(1)
        self.chamber.start()
        time.sleep(2)
        self.chamber.capture_logs()
        self.chamber.set_values(temperature=0, oStart=True)

        self.mFi = mFi("192.168.0.2", "ubnt", "ubnt")
        self.mFi.switchOFF(4)

        time.sleep(1)
        self.mFi.switchON(4)

        self.half = False
        self.all = False

    def run(self):

        while self._work_flag and not self.serial.isOpen():
            try:
                self.serial.open()
            except:
                traceback.print_exc(file=sys.stdout)
                return

        self.response = ""

        while self._work_flag:
            if not self.qcmd.empty():
                self.response = ""
                cmd = self.qcmd.get(False)
                self.serial.write(cmd+'\n')
            try:
                data = self.serial.readline()
                data = data.replace('\r\n', '')
                data = data.replace('\r', '')
                data = data.replace('\n', '')
                #poprawiony filter gdyż zyzek zrobil prawie to samo ale nie do konca
                data = data.replace('Erasing 128 Kibyte @ 0 --  0 % complete Erasing 128 Kibyte @ 20000 --  0 % complete Erasing 128 Kibyte @ 40000 --  0 % complete Erasing 128 Kibyte @ 60000 --  0 % complete Erasing 128 Kibyte @ 80000 --  0 % complete Erasing 128 Kibyte @ a0000 --  0 % complete Erasing 128 Kibyte @ c0000 --  1 % complete Erasing 128 Kibyte @ e0000 --  1 % complete Erasing 128 Kibyte @ 100000 --  1 % complete Erasing 128 Kibyte @ 120000 --  1 % complete Erasing 128 Kibyte @ 140000 --  1 % complete Erasing 128 Kibyte @ 160000 --  2 % complete Erasing 128 Kibyte @ 180000 --  2 % complete Erasing 128 Kibyte @ 1a0000 --  2 % complete Erasing 128 Kibyte @ 1c0000 --  2 % complete Erasing 128 Kibyte @ 1e0000 --  2 % complete Erasing 128 Kibyte @ 200000 --  3 % complete Erasing 128 Kibyte @ 220000 --  3 % complete Erasing 128 Kibyte @ 240000 --  3 % complete Erasing 128 Kibyte @ 260000 --  3 % complete Erasing 128 Kibyte @ 280000 --  3 % complete Erasing 128 Kibyte @ 2a0000 --  4 % complete Erasing 128 Kibyte @ 2c0000 --  4 % complete Erasing 128 Kibyte @ 2e0000 --  4 % complete Erasing 128 Kibyte @ 300000 --  4 % complete Erasing 128 Kibyte @ 320000 --  4 % complete Erasing 128 Kibyte @ 340000 --  5 % complete Erasing 128 Kibyte @ 360000 --  5 % complete Erasing 128 Kibyte @ 380000 --  5 % complete Erasing 128 Kibyte @ 3a0000 --  5 % complete Erasing 128 Kibyte @ 3c0000 --  5 % complete Erasing 128 Kibyte @ 3e0000 --  6 % complete Erasing 128 Kibyte @ 400000 --  6 % complete Erasing 128 Kibyte @ 420000 --  6 % complete Erasing 128 Kibyte @ 440000 --  6 % complete Erasing 128 Kibyte @ 460000 --  6 % complete Erasing 128 Kibyte @ 480000 --  7 % complete Erasing 128 Kibyte @ 4a0000 --  7 % complete Erasing 128 Kibyte @ 4c0000 --  7 % complete Erasing 128 Kibyte @ 4e0000 --  7 % complete Erasing 128 Kibyte @ 500000 --  7 % complete Erasing 128 Kibyte @ 520000 --  8 % complete Erasing 128 Kibyte @ 540000 --  8 % complete Erasing 128 Kibyte @ 560000 --  8 % complete Erasing 128 Kibyte @ 580000 --  8 % complete Erasing 128 Kibyte @ 5a0000 --  8 % complete Erasing 128 Kibyte @ 5c0000 --  8 % complete Erasing 128 Kibyte @ 5e0000 --  9 % complete Erasing 128 Kibyte @ 600000 --  9 % complete Erasing 128 Kibyte @ 620000 --  9 % complete Erasing 128 Kibyte @ 640000 --  9 % complete Erasing 128 Kibyte @ 660000 --  9 % complete Erasing 128 Kibyte @ 680000 -- 10 % complete Erasing 128 Kibyte @ 6a0000 -- 10 % complete Erasing 128 Kibyte @ 6c0000 -- 10 % complete Erasing 128 Kibyte @ 6e0000 -- 10 % complete Erasing 128 Kibyte @ 700000 -- 10 % complete Erasing 128 Kibyte @ 720000 -- 11 % complete Erasing 128 Kibyte @ 740000 -- 11 % complete Erasing 128 Kibyte @ 760000 -- 11 % complete Erasing 128 Kibyte @ 780000 -- 11 % complete Erasing 128 Kibyte @ 7a0000 -- 11 % complete Erasing 128 Kibyte @ 7c0000 -- 12 % complete Erasing 128 Kibyte @ 7e0000 -- 12 % complete Erasing 128 Kibyte @ 800000 -- 12 % complete Erasing 128 Kibyte @ 820000 -- 12 % complete Erasing 128 Kibyte @ 840000 -- 12 % complete Erasing 128 Kibyte @ 860000 -- 13 % complete Erasing 128 Kibyte @ 880000 -- 13 % complete Erasing 128 Kibyte @ 8a0000 -- 13 % complete Erasing 128 Kibyte @ 8c0000 -- 13 % complete Erasing 128 Kibyte @ 8e0000 -- 13 % complete Erasing 128 Kibyte @ 900000 -- 14 % complete Erasing 128 Kibyte @ 920000 -- 14 % complete Erasing 128 Kibyte @ 940000 -- 14 % complete Erasing 128 Kibyte @ 960000 -- 14 % complete Erasing 128 Kibyte @ 980000 -- 14 % complete Erasing 128 Kibyte @ 9a0000 -- 15 % complete Erasing 128 Kibyte @ 9c0000 -- 15 % complete Erasing 128 Kibyte @ 9e0000 -- 15 % complete Erasing 128 Kibyte @ a00000 -- 15 % complete Erasing 128 Kibyte @ a20000 -- 15 % complete Erasing 128 Kibyte @ a40000 -- 16 % complete Erasing 128 Kibyte @ a60000 -- 16 % complete Erasing 128 Kibyte @ a80000 -- 16 % complete Erasing 128 Kibyte @ aa0000 -- 16 % complete Erasing 128 Kibyte @ ac0000 -- 16 % complete Erasing 128 Kibyte @ ae0000 -- 16 % complete Erasing 128 Kibyte @ b00000 -- 17 % complete Erasing 128 Kibyte @ b20000 -- 17 % complete Erasing 128 Kibyte @ b40000 -- 17 % complete Erasing 128 Kibyte @ b60000 -- 17 % complete Erasing 128 Kibyte @ b80000 -- 17 % complete Erasing 128 Kibyte @ ba0000 -- 18 % complete Erasing 128 Kibyte @ bc0000 -- 18 % complete Erasing 128 Kibyte @ be0000 -- 18 % complete Erasing 128 Kibyte @ c00000 -- 18 % complete Erasing 128 Kibyte @ c20000 -- 18 % complete Erasing 128 Kibyte @ c40000 -- 19 % complete Erasing 128 Kibyte @ c60000 -- 19 % complete Erasing 128 Kibyte @ c80000 -- 19 % complete Erasing 128 Kibyte @ ca0000 -- 19 % complete Erasing 128 Kibyte @ cc0000 -- 19 % complete Erasing 128 Kibyte @ ce0000 -- 20 % complete Erasing 128 Kibyte @ d00000 -- 20 % complete Erasing 128 Kibyte @ d20000 -- 20 % complete Erasing 128 Kibyte @ d40000 -- 20 % complete Erasing 128 Kibyte @ d60000 -- 20 % complete Erasing 128 Kibyte @ d80000 -- 21 % complete Erasing 128 Kibyte @ da0000 -- 21 % complete Erasing 128 Kibyte @ dc0000 -- 21 % complete Erasing 128 Kibyte @ de0000 -- 21 % complete Erasing 128 Kibyte @ e00000 -- 21 % complete Erasing 128 Kibyte @ e20000 -- 22 % complete Erasing 128 Kibyte @ e40000 -- 22 % complete Erasing 128 Kibyte @ e60000 -- 22 % complete Erasing 128 Kibyte @ e80000 -- 22 % complete Erasing 128 Kibyte @ ea0000 -- 22 % complete Erasing 128 Kibyte @ ec0000 -- 23 % complete Erasing 128 Kibyte @ ee0000 -- 23 % complete Erasing 128 Kibyte @ f00000 -- 23 % complete Erasing 128 Kibyte @ f20000 -- 23 % complete Erasing 128 Kibyte @ f40000 -- 23 % complete Erasing 128 Kibyte @ f60000 -- 24 % complete Erasing 128 Kibyte @ f80000 -- 24 % complete Erasing 128 Kibyte @ fa0000 -- 24 % complete Erasing 128 Kibyte @ fc0000 -- 24 % complete Erasing 128 Kibyte @ fe0000 -- 24 % complete Erasing 128 Kibyte @ 1000000 -- 25 % complete Erasing 128 Kibyte @ 1020000 -- 25 % complete Erasing 128 Kibyte @ 1040000 -- 25 % complete Erasing 128 Kibyte @ 1060000 -- 25 % complete Erasing 128 Kibyte @ 1080000 -- 25 % complete Erasing 128 Kibyte @ 10a0000 -- 25 % complete Erasing 128 Kibyte @ 10c0000 -- 26 % complete Erasing 128 Kibyte @ 10e0000 -- 26 % complete Erasing 128 Kibyte @ 1100000 -- 26 % complete Erasing 128 Kibyte @ 1120000 -- 26 % complete Erasing 128 Kibyte @ 1140000 -- 26 % complete Erasing 128 Kibyte @ 1160000 -- 27 % complete Erasing 128 Kibyte @ 1180000 -- 27 % complete Erasing 128 Kibyte @ 11a0000 -- 27 % complete Erasing 128 Kibyte @ 11c0000 -- 27 % complete Erasing 128 Kibyte @ 11e0000 -- 27 % complete Erasing 128 Kibyte @ 1200000 -- 28 % complete Erasing 128 Kibyte @ 1220000 -- 28 % complete Erasing 128 Kibyte @ 1240000 -- 28 % complete Erasing 128 Kibyte @ 1260000 -- 28 % complete Erasing 128 Kibyte @ 1280000 -- 28 % complete Erasing 128 Kibyte @ 12a0000 -- 29 % complete Erasing 128 Kibyte @ 12c0000 -- 29 % complete Erasing 128 Kibyte @ 12e0000 -- 29 % complete Erasing 128 Kibyte @ 1300000 -- 29 % complete Erasing 128 Kibyte @ 1320000 -- 29 % complete Erasing 128 Kibyte @ 1340000 -- 30 % complete Erasing 128 Kibyte @ 1360000 -- 30 % complete Erasing 128 Kibyte @ 1380000 -- 30 % complete Erasing 128 Kibyte @ 13a0000 -- 30 % complete Erasing 128 Kibyte @ 13c0000 -- 30 % complete Erasing 128 Kibyte @ 13e0000 -- 31 % complete Erasing 128 Kibyte @ 1400000 -- 31 % complete Erasing 128 Kibyte @ 1420000 -- 31 % complete Erasing 128 Kibyte @ 1440000 -- 31 % complete Erasing 128 Kibyte @ 1460000 -- 31 % complete Erasing 128 Kibyte @ 1480000 -- 32 % complete Erasing 128 Kibyte @ 14a0000 -- 32 % complete Erasing 128 Kibyte @ 14c0000 -- 32 % complete Erasing 128 Kibyte @ 14e0000 -- 32 % complete Erasing 128 Kibyte @ 1500000 -- 32 % complete Erasing 128 Kibyte @ 1520000 -- 33 % complete Erasing 128 Kibyte @ 1540000 -- 33 % complete Erasing 128 Kibyte @ 1560000 -- 33 % complete Erasing 128 Kibyte @ 1580000 -- 33 % complete Erasing 128 Kibyte @ 15a0000 -- 33 % complete Erasing 128 Kibyte @ 15c0000 -- 33 % complete Erasing 128 Kibyte @ 15e0000 -- 34 % complete Erasing 128 Kibyte @ 1600000 -- 34 % complete Erasing 128 Kibyte @ 1620000 -- 34 % complete Erasing 128 Kibyte @ 1640000 -- 34 % complete Erasing 128 Kibyte @ 1660000 -- 34 % complete Erasing 128 Kibyte @ 1680000 -- 35 % complete Erasing 128 Kibyte @ 16a0000 -- 35 % complete Erasing 128 Kibyte @ 16c0000 -- 35 % complete Erasing 128 Kibyte @ 16e0000 -- 35 % complete Erasing 128 Kibyte @ 1700000 -- 35 % complete Erasing 128 Kibyte @ 1720000 -- 36 % complete Erasing 128 Kibyte @ 1740000 -- 36 % complete Erasing 128 Kibyte @ 1760000 -- 36 % complete Erasing 128 Kibyte @ 1780000 -- 36 % complete Erasing 128 Kibyte @ 17a0000 -- 36 % complete Erasing 128 Kibyte @ 17c0000 -- 37 % complete Erasing 128 Kibyte @ 17e0000 -- 37 % complete Erasing 128 Kibyte @ 1800000 -- 37 % complete Erasing 128 Kibyte @ 1820000 -- 37 % complete Erasing 128 Kibyte @ 1840000 -- 37 % complete Erasing 128 Kibyte @ 1860000 -- 38 % complete Erasing 128 Kibyte @ 1880000 -- 38 % complete Erasing 128 Kibyte @ 18a0000 -- 38 % complete Erasing 128 Kibyte @ 18c0000 -- 38 % complete Erasing 128 Kibyte @ 18e0000 -- 38 % complete Erasing 128 Kibyte @ 1900000 -- 39 % complete Erasing 128 Kibyte @ 1920000 -- 39 % complete Erasing 128 Kibyte @ 1940000 -- 39 % complete Erasing 128 Kibyte @ 1960000 -- 39 % complete Erasing 128 Kibyte @ 1980000 -- 39 % complete Erasing 128 Kibyte @ 19a0000 -- 40 % complete Erasing 128 Kibyte @ 19c0000 -- 40 % complete Erasing 128 Kibyte @ 19e0000 -- 40 % complete Erasing 128 Kibyte @ 1a00000 -- 40 % complete Erasing 128 Kibyte @ 1a20000 -- 40 % complete Erasing 128 Kibyte @ 1a40000 -- 41 % complete Erasing 128 Kibyte @ 1a60000 -- 41 % complete Erasing 128 Kibyte @ 1a80000 -- 41 % complete Erasing 128 Kibyte @ 1aa0000 -- 41 % complete Erasing 128 Kibyte @ 1ac0000 -- 41 % complete Erasing 128 Kibyte @ 1ae0000 -- 41 % complete Erasing 128 Kibyte @ 1b00000 -- 42 % complete Erasing 128 Kibyte @ 1b20000 -- 42 % complete Erasing 128 Kibyte @ 1b40000 -- 42 % complete Erasing 128 Kibyte @ 1b60000 -- 42 % complete Erasing 128 Kibyte @ 1b80000 -- 42 % complete Erasing 128 Kibyte @ 1ba0000 -- 43 % complete Erasing 128 Kibyte @ 1bc0000 -- 43 % complete Erasing 128 Kibyte @ 1be0000 -- 43 % complete Erasing 128 Kibyte @ 1c00000 -- 43 % complete Erasing 128 Kibyte @ 1c20000 -- 43 % complete Erasing 128 Kibyte @ 1c40000 -- 44 % complete Erasing 128 Kibyte @ 1c60000 -- 44 % complete Erasing 128 Kibyte @ 1c80000 -- 44 % complete Erasing 128 Kibyte @ 1ca0000 -- 44 % complete Erasing 128 Kibyte @ 1cc0000 -- 44 % complete Erasing 128 Kibyte @ 1ce0000 -- 45 % complete Erasing 128 Kibyte @ 1d00000 -- 45 % complete Erasing 128 Kibyte @ 1d20000 -- 45 % complete Erasing 128 Kibyte @ 1d40000 -- 45 % complete Erasing 128 Kibyte @ 1d60000 -- 45 % complete Erasing 128 Kibyte @ 1d80000 -- 46 % complete Erasing 128 Kibyte @ 1da0000 -- 46 % complete Erasing 128 Kibyte @ 1dc0000 -- 46 % complete Erasing 128 Kibyte @ 1de0000 -- 46 % complete Erasing 128 Kibyte @ 1e00000 -- 46 % complete Erasing 128 Kibyte @ 1e20000 -- 47 % complete Erasing 128 Kibyte @ 1e40000 -- 47 % complete Erasing 128 Kibyte @ 1e60000 -- 47 % complete Erasing 128 Kibyte @ 1e80000 -- 47 % complete Erasing 128 Kibyte @ 1ea0000 -- 47 % complete Erasing 128 Kibyte @ 1ec0000 -- 48 % complete Erasing 128 Kibyte @ 1ee0000 -- 48 % complete Erasing 128 Kibyte @ 1f00000 -- 48 % complete Erasing 128 Kibyte @ 1f20000 -- 48 % complete Erasing 128 Kibyte @ 1f40000 -- 48 % complete Erasing 128 Kibyte @ 1f60000 -- 49 % complete Erasing 128 Kibyte @ 1f80000 -- 49 % complete Erasing 128 Kibyte @ 1fa0000 -- 49 % complete Erasing 128 Kibyte @ 1fc0000 -- 49 % complete Erasing 128 Kibyte @ 1fe0000 -- 49 % complete Erasing 128 Kibyte @ 2000000 -- 50 % complete Erasing 128 Kibyte @ 2020000 -- 50 % complete Erasing 128 Kibyte @ 2040000 -- 50 % complete Erasing 128 Kibyte @ 2060000 -- 50 % complete Erasing 128 Kibyte @ 2080000 -- 50 % complete Erasing 128 Kibyte @ 20a0000 -- 50 % complete Erasing 128 Kibyte @ 20c0000 -- 51 % complete Erasing 128 Kibyte @ 20e0000 -- 51 % complete Erasing 128 Kibyte @ 2100000 -- 51 % complete Erasing 128 Kibyte @ 2120000 -- 51 % complete Erasing 128 Kibyte @ 2140000 -- 51 % complete Erasing 128 Kibyte @ 2160000 -- 52 % complete Erasing 128 Kibyte @ 2180000 -- 52 % complete Erasing 128 Kibyte @ 21a0000 -- 52 % complete Erasing 128 Kibyte @ 21c0000 -- 52 % complete Erasing 128 Kibyte @ 21e0000 -- 52 % complete Erasing 128 Kibyte @ 2200000 -- 53 % complete Erasing 128 Kibyte @ 2220000 -- 53 % complete Erasing 128 Kibyte @ 2240000 -- 53 % complete Erasing 128 Kibyte @ 2260000 -- 53 % complete Erasing 128 Kibyte @ 2280000 -- 53 % complete Erasing 128 Kibyte @ 22a0000 -- 54 % complete Erasing 128 Kibyte @ 22c0000 -- 54 % complete Erasing 128 Kibyte @ 22e0000 -- 54 % complete Erasing 128 Kibyte @ 2300000 -- 54 % complete Erasing 128 Kibyte @ 2320000 -- 54 % complete Erasing 128 Kibyte @ 2340000 -- 55 % complete Erasing 128 Kibyte @ 2360000 -- 55 % complete Erasing 128 Kibyte @ 2380000 -- 55 % complete Erasing 128 Kibyte @ 23a0000 -- 55 % complete Erasing 128 Kibyte @ 23c0000 -- 55 % complete Erasing 128 Kibyte @ 23e0000 -- 56 % complete Erasing 128 Kibyte @ 2400000 -- 56 % complete Erasing 128 Kibyte @ 2420000 -- 56 % complete Erasing 128 Kibyte @ 2440000 -- 56 % complete Erasing 128 Kibyte @ 2460000 -- 56 % complete Erasing 128 Kibyte @ 2480000 -- 57 % complete Erasing 128 Kibyte @ 24a0000 -- 57 % complete Erasing 128 Kibyte @ 24c0000 -- 57 % complete Erasing 128 Kibyte @ 24e0000 -- 57 % complete Erasing 128 Kibyte @ 2500000 -- 57 % complete Erasing 128 Kibyte @ 2520000 -- 58 % complete Erasing 128 Kibyte @ 2540000 -- 58 % complete Erasing 128 Kibyte @ 2560000 -- 58 % complete Erasing 128 Kibyte @ 2580000 -- 58 % complete Erasing 128 Kibyte @ 25a0000 -- 58 % complete Erasing 128 Kibyte @ 25c0000 -- 58 % complete Erasing 128 Kibyte @ 25e0000 -- 59 % complete Erasing 128 Kibyte @ 2600000 -- 59 % complete Erasing 128 Kibyte @ 2620000 -- 59 % complete Erasing 128 Kibyte @ 2640000 -- 59 % complete Erasing 128 Kibyte @ 2660000 -- 59 % complete Erasing 128 Kibyte @ 2680000 -- 60 % complete Erasing 128 Kibyte @ 26a0000 -- 60 % complete Erasing 128 Kibyte @ 26c0000 -- 60 % complete Erasing 128 Kibyte @ 26e0000 -- 60 % complete Erasing 128 Kibyte @ 2700000 -- 60 % complete Erasing 128 Kibyte @ 2720000 -- 61 % complete Erasing 128 Kibyte @ 2740000 -- 61 % complete Erasing 128 Kibyte @ 2760000 -- 61 % complete Erasing 128 Kibyte @ 2780000 -- 61 % complete Erasing 128 Kibyte @ 27a0000 -- 61 % complete Erasing 128 Kibyte @ 27c0000 -- 62 % complete Erasing 128 Kibyte @ 27e0000 -- 62 % complete Erasing 128 Kibyte @ 2800000 -- 62 % complete Erasing 128 Kibyte @ 2820000 -- 62 % complete Erasing 128 Kibyte @ 2840000 -- 62 % complete Erasing 128 Kibyte @ 2860000 -- 63 % complete Erasing 128 Kibyte @ 2880000 -- 63 % complete Erasing 128 Kibyte @ 28a0000 -- 63 % complete Erasing 128 Kibyte @ 28c0000 -- 63 % complete Erasing 128 Kibyte @ 28e0000 -- 63 % complete Erasing 128 Kibyte @ 2900000 -- 64 % complete Erasing 128 Kibyte @ 2920000 -- 64 % complete Erasing 128 Kibyte @ 2940000 -- 64 % complete Erasing 128 Kibyte @ 2960000 -- 64 % complete Erasing 128 Kibyte @ 2980000 -- 64 % complete Erasing 128 Kibyte @ 29a0000 -- 65 % complete Erasing 128 Kibyte @ 29c0000 -- 65 % complete Erasing 128 Kibyte @ 29e0000 -- 65 % complete Erasing 128 Kibyte @ 2a00000 -- 65 % complete Erasing 128 Kibyte @ 2a20000 -- 65 % complete Erasing 128 Kibyte @ 2a40000 -- 66 % complete Erasing 128 Kibyte @ 2a60000 -- 66 % complete Erasing 128 Kibyte @ 2a80000 -- 66 % complete Erasing 128 Kibyte @ 2aa0000 -- 66 % complete Erasing 128 Kibyte @ 2ac0000 -- 66 % complete Erasing 128 Kibyte @ 2ae0000 -- 66 % complete Erasing 128 Kibyte @ 2b00000 -- 67 % complete Erasing 128 Kibyte @ 2b20000 -- 67 % complete Erasing 128 Kibyte @ 2b40000 -- 67 % complete Erasing 128 Kibyte @ 2b60000 -- 67 % complete Erasing 128 Kibyte @ 2b80000 -- 67 % complete Erasing 128 Kibyte @ 2ba0000 -- 68 % complete Erasing 128 Kibyte @ 2bc0000 -- 68 % complete Erasing 128 Kibyte @ 2be0000 -- 68 % complete Erasing 128 Kibyte @ 2c00000 -- 68 % complete Erasing 128 Kibyte @ 2c20000 -- 68 % complete Erasing 128 Kibyte @ 2c40000 -- 69 % complete Erasing 128 Kibyte @ 2c60000 -- 69 % complete Erasing 128 Kibyte @ 2c80000 -- 69 % complete Erasing 128 Kibyte @ 2ca0000 -- 69 % complete Erasing 128 Kibyte @ 2cc0000 -- 69 % complete Erasing 128 Kibyte @ 2ce0000 -- 70 % complete Erasing 128 Kibyte @ 2d00000 -- 70 % complete Erasing 128 Kibyte @ 2d20000 -- 70 % complete Erasing 128 Kibyte @ 2d40000 -- 70 % complete Erasing 128 Kibyte @ 2d60000 -- 70 % complete Erasing 128 Kibyte @ 2d80000 -- 71 % complete Erasing 128 Kibyte @ 2da0000 -- 71 % complete Erasing 128 Kibyte @ 2dc0000 -- 71 % complete Erasing 128 Kibyte @ 2de0000 -- 71 % complete Erasing 128 Kibyte @ 2e00000 -- 71 % complete Erasing 128 Kibyte @ 2e20000 -- 72 % complete Erasing 128 Kibyte @ 2e40000 -- 72 % complete Erasing 128 Kibyte @ 2e60000 -- 72 % complete Erasing 128 Kibyte @ 2e80000 -- 72 % complete Erasing 128 Kibyte @ 2ea0000 -- 72 % complete Erasing 128 Kibyte @ 2ec0000 -- 73 % complete Erasing 128 Kibyte @ 2ee0000 -- 73 % complete Erasing 128 Kibyte @ 2f00000 -- 73 % complete Erasing 128 Kibyte @ 2f20000 -- 73 % complete Erasing 128 Kibyte @ 2f40000 -- 73 % complete Erasing 128 Kibyte @ 2f60000 -- 74 % complete Erasing 128 Kibyte @ 2f80000 -- 74 % complete Erasing 128 Kibyte @ 2fa0000 -- 74 % complete Erasing 128 Kibyte @ 2fc0000 -- 74 % complete Erasing 128 Kibyte @ 2fe0000 -- 74 % complete Erasing 128 Kibyte @ 3000000 -- 75 % complete Erasing 128 Kibyte @ 3020000 -- 75 % complete Erasing 128 Kibyte @ 3040000 -- 75 % complete Erasing 128 Kibyte @ 3060000 -- 75 % complete Erasing 128 Kibyte @ 3080000 -- 75 % complete Erasing 128 Kibyte @ 30a0000 -- 75 % complete Erasing 128 Kibyte @ 30c0000 -- 76 % complete Erasing 128 Kibyte @ 30e0000 -- 76 % complete Erasing 128 Kibyte @ 3100000 -- 76 % complete Erasing 128 Kibyte @ 3120000 -- 76 % complete Erasing 128 Kibyte @ 3140000 -- 76 % complete Erasing 128 Kibyte @ 3160000 -- 77 % complete Erasing 128 Kibyte @ 3180000 -- 77 % complete Erasing 128 Kibyte @ 31a0000 -- 77 % complete Erasing 128 Kibyte @ 31c0000 -- 77 % complete Erasing 128 Kibyte @ 31e0000 -- 77 % complete Erasing 128 Kibyte @ 3200000 -- 78 % complete Erasing 128 Kibyte @ 3220000 -- 78 % complete Erasing 128 Kibyte @ 3240000 -- 78 % complete Erasing 128 Kibyte @ 3260000 -- 78 % complete Erasing 128 Kibyte @ 3280000 -- 78 % complete Erasing 128 Kibyte @ 32a0000 -- 79 % complete Erasing 128 Kibyte @ 32c0000 -- 79 % complete Erasing 128 Kibyte @ 32e0000 -- 79 % complete Erasing 128 Kibyte @ 3300000 -- 79 % complete Erasing 128 Kibyte @ 3320000 -- 79 % complete Erasing 128 Kibyte @ 3340000 -- 80 % complete Erasing 128 Kibyte @ 3360000 -- 80 % complete Erasing 128 Kibyte @ 3380000 -- 80 % complete Erasing 128 Kibyte @ 33a0000 -- 80 % complete Erasing 128 Kibyte @ 33c0000 -- 80 % complete Erasing 128 Kibyte @ 33e0000 -- 81 % complete Erasing 128 Kibyte @ 3400000 -- 81 % complete Erasing 128 Kibyte @ 3420000 -- 81 % complete Erasing 128 Kibyte @ 3440000 -- 81 % complete Erasing 128 Kibyte @ 3460000 -- 81 % complete Erasing 128 Kibyte @ 3480000 -- 82 % complete Erasing 128 Kibyte @ 34a0000 -- 82 % complete Erasing 128 Kibyte @ 34c0000 -- 82 % complete Erasing 128 Kibyte @ 34e0000 -- 82 % complete Erasing 128 Kibyte @ 3500000 -- 82 % complete Erasing 128 Kibyte @ 3520000 -- 83 % complete Erasing 128 Kibyte @ 3540000 -- 83 % complete Erasing 128 Kibyte @ 3560000 -- 83 % complete Erasing 128 Kibyte @ 3580000 -- 83 % complete Erasing 128 Kibyte @ 35a0000 -- 83 % complete Erasing 128 Kibyte @ 35c0000 -- 83 % complete Erasing 128 Kibyte @ 35e0000 -- 84 % complete Erasing 128 Kibyte @ 3600000 -- 84 % complete Erasing 128 Kibyte @ 3620000 -- 84 % complete Erasing 128 Kibyte @ 3640000 -- 84 % complete Erasing 128 Kibyte @ 3660000 -- 84 % complete Erasing 128 Kibyte @ 3680000 -- 85 % complete Erasing 128 Kibyte @ 36a0000 -- 85 % complete Erasing 128 Kibyte @ 36c0000 -- 85 % complete Erasing 128 Kibyte @ 36e0000 -- 85 % complete Erasing 128 Kibyte @ 3700000 -- 85 % complete Erasing 128 Kibyte @ 3720000 -- 86 % complete Erasing 128 Kibyte @ 3740000 -- 86 % complete Erasing 128 Kibyte @ 3760000 -- 86 % complete Erasing 128 Kibyte @ 3780000 -- 86 % complete Erasing 128 Kibyte @ 37a0000 -- 86 % complete Erasing 128 Kibyte @ 37c0000 -- 87 % complete Erasing 128 Kibyte @ 37e0000 -- 87 % complete Erasing 128 Kibyte @ 3800000 -- 87 % complete Erasing 128 Kibyte @ 3820000 -- 87 % complete Erasing 128 Kibyte @ 3840000 -- 87 % complete Erasing 128 Kibyte @ 3860000 -- 88 % complete Erasing 128 Kibyte @ 3880000 -- 88 % complete Erasing 128 Kibyte @ 38a0000 -- 88 % complete Erasing 128 Kibyte @ 38c0000 -- 88 % complete Erasing 128 Kibyte @ 38e0000 -- 88 % complete Erasing 128 Kibyte @ 3900000 -- 89 % complete Erasing 128 Kibyte @ 3920000 -- 89 % complete Erasing 128 Kibyte @ 3940000 -- 89 % complete Erasing 128 Kibyte @ 3960000 -- 89 % complete Erasing 128 Kibyte @ 3980000 -- 89 % complete Erasing 128 Kibyte @ 39a0000 -- 90 % complete Erasing 128 Kibyte @ 39c0000 -- 90 % complete Erasing 128 Kibyte @ 39e0000 -- 90 % complete Erasing 128 Kibyte @ 3a00000 -- 90 % complete Erasing 128 Kibyte @ 3a20000 -- 90 % complete Erasing 128 Kibyte @ 3a40000 -- 91 % complete Erasing 128 Kibyte @ 3a60000 -- 91 % complete Erasing 128 Kibyte @ 3a80000 -- 91 % complete Erasing 128 Kibyte @ 3aa0000 -- 91 % complete Erasing 128 Kibyte @ 3ac0000 -- 91 % complete Erasing 128 Kibyte @ 3ae0000 -- 91 % complete Erasing 128 Kibyte @ 3b00000 -- 92 % complete Erasing 128 Kibyte @ 3b20000 -- 92 % complete Erasing 128 Kibyte @ 3b40000 -- 92 % complete Erasing 128 Kibyte @ 3b60000 -- 92 % complete Erasing 128 Kibyte @ 3b80000 -- 92 % complete Erasing 128 Kibyte @ 3ba0000 -- 93 % complete Erasing 128 Kibyte @ 3bc0000 -- 93 % complete Erasing 128 Kibyte @ 3be0000 -- 93 % complete Erasing 128 Kibyte @ 3c00000 -- 93 % complete Erasing 128 Kibyte @ 3c20000 -- 93 % complete Erasing 128 Kibyte @ 3c40000 -- 94 % complete Erasing 128 Kibyte @ 3c60000 -- 94 % complete Erasing 128 Kibyte @ 3c80000 -- 94 % complete Erasing 128 Kibyte @ 3ca0000 -- 94 % complete Erasing 128 Kibyte @ 3cc0000 -- 94 % complete Erasing 128 Kibyte @ 3ce0000 -- 95 % complete Erasing 128 Kibyte @ 3d00000 -- 95 % complete Erasing 128 Kibyte @ 3d20000 -- 95 % complete Erasing 128 Kibyte @ 3d40000 -- 95 % complete Erasing 128 Kibyte @ 3d60000 -- 95 % complete Erasing 128 Kibyte @ 3d80000 -- 96 % complete Erasing 128 Kibyte @ 3da0000 -- 96 % complete Erasing 128 Kibyte @ 3dc0000 -- 96 % complete Erasing 128 Kibyte @ 3de0000 -- 96 % complete Erasing 128 Kibyte @ 3e00000 -- 96 % complete Erasing 128 Kibyte @ 3e20000 -- 97 % complete Erasing 128 Kibyte @ 3e40000 -- 97 % complete Erasing 128 Kibyte @ 3e60000 -- 97 % complete Erasing 128 Kibyte @ 3e80000 -- 97 % complete Erasing 128 Kibyte @ 3ea0000 -- 97 % complete Erasing 128 Kibyte @ 3ec0000 -- 98 % complete Erasing 128 Kibyte @ 3ee0000 -- 98 % complete Erasing 128 Kibyte @ 3f00000 -- 98 % complete Erasing 128 Kibyte @ 3f20000 -- 98 % complete Erasing 128 Kibyte @ 3f40000 -- 98 % complete Erasing 128 Kibyte @ 3f60000 -- 99 % complete Erasing 128 Kibyte @ 3f80000 -- 99 % complete Erasing 128 Kibyte @ 3fa0000 -- 99 % complete Erasing 128 Kibyte @ 3fc0000 -- 99 % complete Erasing 128 Kibyte @ 3fe0000 -- 99 % complete ','')
                data = data.replace('0 % complete Erasing 128 Kibyte @ 0 --','')

                if "Start skryptu" in data:
                    self.response = ""
                    self.counter += 1
                    self.log_flag = True

                if self.log_flag:
                    out = data.replace('\b', '')

                    redata = re.sub(r'(\\\|/-)', "", out)
                    if redata != data:
                        out = re.sub(r'(\s+)', " ", redata)
                        out = re.sub(r'\|', "", out)

                    redata2 = re.sub(r'(setting+\s+\d+testing+\s+\d+)', "", out)
                    if redata2 != data:
                        out = re.sub(r'(\s+)', " ", redata2)

                    self.response += out
                    self.response += "\n"

                if "Cykl testu" in data:
                    self.log_flag = False

                    with open("flash_test_log.log", "a") as myfile:
                        if "negatywny" in self.response:
                            myfile.write("TEST : "+str(self.counter)+" ERROR "+str(datetime.datetime.now())+"\n")
                            print str(self.counter)+" ERROR  "+str(datetime.datetime.now())
                        else:
                            myfile.write("TEST : "+str(self.counter)+" OK "+str(datetime.datetime.now())+"\n")
                            print str(self.counter)+" OK  "+str(datetime.datetime.now())
                        myfile.write("\t"+self.response+"\n\n")

                if (self.counter == 500) and not self.half:
                    with open("flash_test_log.log", "a") as myfile:
                        myfile.write("\n\n-----------------------\n\n")
                    self.mFi.switchOFF(4)
                    self.chamber.set_values(temperature=45, oStart=True)
                    time.sleep(15*60)
                    time.sleep(2)
                    self.mFi.switchON(4)
                    self.half = True

                if (self.counter == 1000) and not self.all:
                    self.mFi.switchOFF(4)
                    self.chamber.set_values(temperature=45, oStart=False)
                    self.stop()
                    self.all = True
            except:
                with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())

    def send(self, cmd):
        print u">", cmd
        self.qcmd.put(cmd)

    def clearQ(self):
        with self.qcmd.mutex:
            self.qcmd.queue.clear()

    def stop(self):
        self._work_flag = False
        self.serial.close()

if __name__ == "__main__":
    print u"Rozpoczecie testu", datetime.datetime.now()
    st = COM(115200, "/dev/ttyUSB1", 30)
    #st.start()
    st.run()

    #time.sleep(24*60*60)
