# -*- coding: utf-8 -*-
# PYTHONPATH="/home/cp/stb/lib"

import datetime
from COM import *
from chroma import *
from pyBMD import *
from WKL64 import *
import os
from sys import platform as _platform
import sys


FRAME = {"valid": 0, "invalid": 0}


def ping(hostname):
    if _platform == "linux" or _platform == "linux2":
        return os.system("ping -c 4 " + hostname)
    elif _platform == "win32":
        return os.system("ping " + hostname)


def callback_frame(width, height, bytes_in_row, pixel_format, buff_p):
    #print "dev_id: %d, w: %d, h: %d, inrow: %d" % (
        #    BMD_id, width, height, bytes_in_row)
    return True


def callback_start():
    #print "\tStart capturing frames"
    return True


def callback_stop(infalidnb, validnb):
    print "\tInvalid: %s, Valid: %s" % (infalidnb, validnb)
    FRAME["valid"] = validnb
    FRAME["invalid"] = infalidnb
    return True


if __name__ == "__main__":
    USB_STB = "/dev/ttyUSB1"
    USB_CHROMA = "/dev/ttyUSB0"
    USB_CHAMBER = "/dev/ttyUSB2"
    frame_threshold = 0.99

    Ton = 60

    Tmin = 5
    Tmax = 40
    Vnom_min = 230
    Vnom_max = 230
    Fnom_min = 50
    Fnom_max = 50

    TESTS = [{
            "Ton": 60,
            "Toff": 60,
            "temperature": Tmin-5,
            "volts": Vnom_min - 0.1*Vnom_min,
            "frequency": Fnom_min-3,
            "logfile": "test 3.2 Power On Off_A.log"},
        {
            "Ton": 60,
            "Toff": 60,
            "temperature": Tmin-5,
            "volts": Vnom_max + 0.1*Vnom_max,
            "frequency": Fnom_max+3,
            "logfile": "test 3.2 Power On Off_B.log"},
        {
            "Ton": 60,
            "Toff": 1,
            "temperature": Tmax+5,
            "volts": Vnom_min - 0.1*Vnom_min,
            "frequency": Fnom_min-3,
            "logfile": "test 3.2 Power On Off_C.log"},
        {
            "Ton": 60,
            "Toff": 1,
            "temperature": Tmax+5,
            "volts": Vnom_max + 0.1*Vnom_max,
            "frequency": Fnom_max+3,
            "logfile": "test 3.2 Power On Off_D.log"},
        {
            "Ton": 60,
            "Toff": 1,
            "temperature": 25,
            "volts": 230,
            "frequency": 50,
            "logfile": "test 3.2 Power On Off_E.log"},
    ]

    chroma = CHROMA(19200, USB_CHROMA, 1)
    chroma.start()
    chroma.send("OUTPut OFF")
    chamber = WKL64(19200, USB_CHAMBER, bus=1)
    chamber.start()
    time.sleep(1)
    chamber.capture_logs()

    BMD_id = 0
    bmd = BMD(BMD_id)
    bmd.setStartLoopCallback(callback_start)
    bmd.setStopLoopCallback(callback_stop)
    bmd.setFrameArrivedCallback(callback_frame)

    for test in TESTS:
        with open(test["logfile"], "a") as myfile:
            myfile.write("TRD 3.2 AC Power ON/OFF\n")
            myfile.write("\ttemperature: %sst\n" % test["temperature"])
            myfile.write("\tAC: %sV\n" % test["volts"])
            myfile.write("\tFrequency: %sHz\n\n" % test["frequency"])

        chamber.set_values(temperature=test["temperature"], oStart=True)
        chroma.send("VOLT:AC %s" % test["volts"])
        chroma.send("FREQ %s" % test["frequency"])

        time.sleep(15*60)  # chamber termalization
        stb = None
        TEST_SUCCESS = True
        errors = 0
        for idx in xrange(500):
            try:
                print "TEST %s %s" % (idx, datetime.datetime.now())
                with open(test["logfile"], "a") as myfile:
                    myfile.write("TEST %s %s" % (idx, datetime.datetime.now()))

                chroma.send("OUTPut ON")
                stb = COM(115200, USB_STB, 3, "com_log_file.log")
                stb.start()
                time.sleep(25)
                TEST_SUCCESS = True

                stb.send("transponder 0 802000 6875 q256")
                time.sleep(1)
                stb.send("avp_pids_set 0x204 0x201 0x201")
                time.sleep(1)
                stb.send("video_decoder 1")
                time.sleep(1)
                stb.send("audio_decoder 2")
                time.sleep(1)
                with open(test["logfile"], "a") as myfile:
                    myfile.write("\n\tRESTART STB")
                    myfile.write("\n\tSend: transponder 0 802000 6875 q256")
                    myfile.write("\n\tSend: avp_pids_set 0x204 0x201 0x201")
                    myfile.write("\n\tSend: video_decoder 1")
                    myfile.write("\n\tSend: audio_decoder 2")
                    myfile.write("\n\tSend: tats 0\n")
                # ######################## TATS 0 ########################
                stb.send("tats 0")
                time.sleep(4)
                response = stb.get_response()
                if "SPECTRUM" in response:
                    print "\tTATS - OK"
                    with open(test["logfile"], "a") as myfile:
                        tmp = '\n\t  '.join(response.split('\n')[1:])
                        tmp = tmp.replace('\r', '')
                        myfile.write("\t%s" % tmp)
                        myfile.write("\n\tTATS 0 - OK")
                else:
                    TEST_SUCCESS = False
                    print "\tTATS - ERROR"
                    with open(test["logfile"], "a") as myfile:
                        tmp = '\t  '.join(response.split('\n')[1:])
                        myfile.write("\t%s" % tmp)
                        myfile.write("\n\tTATS 0 - ERROR")
                # ######################## SET DHCP ######################
                stb.send("netdhcp 2 1")
                time.sleep(10)
                stb.send("netstatus 2")
                time.sleep(2)
                response = stb.get_response()
                with open(test["logfile"], "a") as myfile:
                    myfile.write("\n\n\tNETSTATUS 2:\n")
                    tmp = '\n\t  '.join(response.split('\n')[1:])
                    myfile.write("\t%s" % tmp)
                if "boot.prot" in response:
                    with open(test["logfile"], "a") as myfile:
                        myfile.write("\n\tNETSTATUS 2 - OK")
                else:
                    TEST_SUCCESS = False
                    with open(test["logfile"], "a") as myfile:
                        myfile.write("\n\tNETSTATUS 2 - ERROR")

                hostname = []
                try:
                    hostname = re.findall(r'ip:\s+(192.168.\d+.\d+)', response)
                except:
                    print "ERRR - hostname"
                    hostname = ["192.168.1.100"]

                # ######################## PING ###################### 4s
                if hostname[0] != "192.168.10.100":
                    response = ping(hostname[0])
                    if response == 0:
                        print "\tPING", hostname[0], " - OK"
                        with open(test["logfile"], "a") as myfile:
                            myfile.write("\n\tPING" + hostname[0] + " - OK")
                    else:
                        TEST_SUCCESS = False
                        print "\tPING", hostname[0], " ERROR"
                        with open(test["logfile"], "a") as myfile:
                            myfile.write("\n\tPING - ERROR")
                else:
                    TEST_SUCCESS = False
                    with open(test["logfile"], "a") as myfile:
                        myfile.write("\n\tPING - ERROR - not working dhcp")
                    print "\tPING - ERROR - not working dhcp"

                # ######################## DeckLink ######################

                bmd.startLoop(100)
                if float(((FRAME["valid"]+1)/(FRAME["valid"]+FRAME["invalid"]))) >= frame_threshold:
                    print "VIDEO out - OK"
                    with open(test["logfile"], "a") as myfile:
                        myfile.write("\n\n\tValid: frames: %d" % (FRAME["valid"]+1))
                        myfile.write("\n\tInvalid: frames: %d" % (FRAME["invalid"]-1))
                        myfile.write("\n\tVIDEO - OK")
                else:
                    TEST_SUCCESS = False
                    print "VIDEO out - ERROR"
                    with open(test["logfile"], "a") as myfile:
                        myfile.write("\n\n\tValid: frames: %d" % (FRAME["valid"]+1))
                        myfile.write("\n\tInvalid: frames: %d" % (FRAME["invalid"]-1))
                        myfile.write("\n\tVIDEO - ERROR")

                time.sleep(test["Ton"] - 52)
            except:
                TEST_SUCCESS = False
                print "MAIN LOOP", traceback.format_exc()
            finally:
                chroma.send("OUTPut OFF")
                if stb:
                    stb.stop()
                if not TEST_SUCCESS:
                    errors += 1
                time.sleep(test["Toff"])

        with open(test["logfile"], "a") as myfile:
            myfile.write("\n\n-------------------------------------------\n\n")
            myfile.write("Errors: %s" % errors)

    bmd.stopLoop()
    time.sleep(1)
    bmd.deleta()

    chamber.set_values(temperature=25, oStart=False)
    chamber.stop_capturing_logs()
    time.sleep(1)
    chamber.stop()

    chroma.stop()