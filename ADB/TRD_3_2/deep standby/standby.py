# -*- coding: utf-8 -*-
import datetime
import time
import json
from COM import *
from mFi import *
from WKL64 import *
from ADBlib import *
from pyBMD import *


FRAME = {"valid": 0, "invalid": 0}


def callback_frame(width, height, bytes_in_row, pixel_format, buff_p):
    return True


def callback_start():
    return True


def callback_stop(infalidnb, validnb):
    print "\tInvalid: %s, Valid: %s" % (infalidnb, validnb)
    FRAME["valid"] = validnb
    FRAME["invalid"] = infalidnb
    return True

if __name__ == "__main__":
    #Ton = 5 * 60
    #Toff = 10 * 60
    USB_STB = "/dev/ttyUSB0"
    ETH_MFI = "192.168.0.2"
    USB_CHAMBER = "/dev/ttyUSB2"
    MFI_SOCKET = 4
    filename = "deep_standby_logs.log"
    frame_threshold = 0.99
    #  2h -5 st

    TESTS = [{
            "Tw": 1,
            "temperature": 0,
            "tremalization": 0,
            "logfile": "test 3.2 Deep Standby_A.log"},
        {
            "Tw": 1,
            "temperature": 45,
            "tremalization": 15*60,
            "logfile": "test 3.2 Deep Standby_B.log"},
        {
            "Tw": 1,
            "temperature": 25,
            "tremalization": 15*60,
            "logfile": "test 3.2 Deep Standby_C.log"}
    ]

    mFi = mFi(ETH_MFI, "ubnt", "ubnt")
    mFi.switchON(MFI_SOCKET)

    BMD_id = 0
    bmd = BMD(BMD_id)
    bmd.setStartLoopCallback(callback_start)
    bmd.setStopLoopCallback(callback_stop)
    bmd.setFrameArrivedCallback(callback_frame)

    chamber = WKL64(19200, USB_CHAMBER, bus=1)
    chamber.start()
    time.sleep(2)
    chamber.capture_logs()

    TEST_SUCCESS = True
    PREV_TEST = True
    for test in TESTS:
        chamber.set_values(temperature=test["temperature"], oStart=True)
        time.sleep(test["tremalization"])

        stb = None
        CYCLES = 0
        ERRORS = 0
        for i in xrange(500):
            try:
                Decklink_OK = True
                TEST_SUCCESS = True
                print "TEST %s %s" % (CYCLES, datetime.datetime.now())
                with open(filename, "a") as myfile:
                    myfile.write("\nTEST %s %s\n" % (CYCLES, datetime.datetime.now()))

                stb = COM(115200, USB_STB, 0, "com_log.log")
                stb.start()
                time.sleep(30)
                stb.send("transponder 0 802000 6875 q256")
                time.sleep(1)
                stb.send("avp_pids_set 0x101 0x100 0x100")   # avp_pids_set 0x204 0x201 0x201   257
                time.sleep(1)
                stb.send("video_decoder 3")
                time.sleep(1)
                stb.send("audio_decoder 2")
                time.sleep(1)

                # ######################## TATS 0 ########################
                if not check_tats(stb, filename):
                    TEST_SUCCESS = False
                # ######################## SET DHCP ######################
                if not setDHCPandPING(stb, filename):
                    TEST_SUCCESS = False
                # ######################## DECKLINK ######################

                bmd.startLoop(100)
                if FRAME["invalid"] == 1:
                    FRAME["invalid"] = FRAME["invalid"]-1
                    FRAME["valid"] += 1
                with open(filename, "a") as myfile:
                    myfile.write("\n\n\tValid: frames: %d" % (FRAME["valid"]))
                    myfile.write("\n\tInvalid: frames: %d" % (FRAME["invalid"]))
                    try:
                        myfile.write("\n\tVALID/ALL: %s" % float(((FRAME["valid"]+1)/(FRAME["valid"]+FRAME["invalid"]))))
                    except:
                        pass

                try:
                    if float(((FRAME["valid"])/(FRAME["valid"]+FRAME["invalid"]))) >= frame_threshold:
                        Decklink_OK = True
                    else:
                        Decklink_OK = False
                        TEST_SUCCESS = False
                except:
                    with open("script_error.log", "a") as myfile:
                        myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                        myfile.write("\n%s\n" % traceback.format_exc())

            except:
                with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())
            finally:

                if Decklink_OK is True:
                    print "\tVIDEO - OK"
                    with open(filename, "a") as myfile:
                        myfile.write("\n\tVIDEO - OK")
                else:
                    TEST_SUCCESS = False
                    print "\tVIDEO - ERROR"
                    with open(filename, "a") as myfile:
                        myfile.write("\n\tVIDEO - ERROR")

                if not PREV_TEST and not TEST_SUCCESS:
                    mFi.switchOFF(MFI_SOCKET)
                    time.sleep(2)
                    mFi.switchON(MFI_SOCKET)
                    time.sleep(30)
                stb.send("deep_standby 1 1")
                time.sleep(2)
                if stb:
                    stb.stop()
                if not TEST_SUCCESS:
                    ERRORS += 1
                    print "\tWAKE UP - ERROR"
                    with open(filename, "a") as myfile:
                        myfile.write("\n\tWAKE UP - ERROR")
                else:
                    print "\tWAKE UP - OK"
                    with open(filename, "a") as myfile:
                        myfile.write("\n\tWAKE UP - OK")
                CYCLES += 1
                PREV_TEST = TEST_SUCCESS

                time.sleep(120)

        with open(filename, "a") as myfile:
            myfile.write("\n\n---------------------------------------\n\n")
            myfile.write("Cycle: %s\n" % CYCLES)
            myfile.write("Errors: %s" % ERRORS)

    chamber.stop_capturing_logs()
    chamber.stop()