# SAUT-STB-TESTER #

Zbiór samodzielnych skryptów, których zdaniem jest testowanie prostych, acz długotrwałych właściowści STB (głównie stabilność pracy).
Całość funkcjonalności została zmigrowana i zaimplementowana ponownie w ramach projektu [ATMS](https://bitbucket.org/zpomianowski/atms).
Ten repo ma już jedynie charakter archiwum dla starych testów.

### Konfiguracja ###
* Python 
```
#!bash
# ważne
sudo apt-get install python-pip

# dodatkowe, głównie pod kątem edycji w atom.io
sudo apt-get install python-flake8
sudo apt-get install python-jedi

```

* Instalacja zależności pythona

```
#!bash
sudo pip install -r requirements.txt

```