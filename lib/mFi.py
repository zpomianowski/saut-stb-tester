# -*- coding: utf-8 -*-
u'''
Komunikacja SSH mFI
pip install paramiko
'''
import time
import datetime
import paramiko
import base64
import traceback


class mFi():
    def __init__(self, addr, user, password):
        self.addr = addr
        self.user = user
        self.password = password
        self._work_flag = True
        self.ssh = None

        self.ssh = paramiko.SSHClient()
        self.ssh.load_system_host_keys()
        self.ssh.set_missing_host_key_policy(paramiko.WarningPolicy())
        self.ssh.connect(self.addr, username=self.user, password=self.password)

    def send(self, cmd):
        self.ssh.exec_command(cmd)

    def switchON(self, socket):
        if 0 < socket < 7:
            self.ssh.exec_command("echo 1 > /dev/output%s" % socket)

    def switchOFF(self, socket):
        if 0 < socket < 7:
            self.ssh.exec_command("echo 0 > /dev/output%s" % socket)

    def stop(self):
        self.ssh.close()

if __name__ == "__main__":
    ssh = mFi("192.168.2.3", "ubnt", "ubnt")
    ssh.switchON(1)
    time.sleep(5)
    ssh.switchOFF(1)
    time.sleep(1)
    ssh.stop()
