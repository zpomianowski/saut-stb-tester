# -*- coding: utf-8 -*-
u'''
Sterowanie komorą termiczną WKL-64
1. Adres komory 01
2. Baudrate 19200
3. Protokół ASCII-2

'''

import serial
import Queue
from threading import Thread
import threading
import sys
import traceback
import time
import math
import re
import datetime


class WKL64(Thread):
    def __init__(self, baudrate=19200, com='/dev/ttyUSB0', bus=1,
                 log_filename="measure_log.log"):
        Thread.__init__(self)
        self.serial = serial.Serial()
        self.serial.baudrate = baudrate
        self.serial.port = com
        self.serial.parity = serial.PARITY_NONE
        self.serial.stopbits = serial.STOPBITS_ONE
        self.serial.bytesize = serial.EIGHTBITS
        serial.timeout = 2

        self._work_flag = True
        self.delay = 1  # czekamy na odpowiedź komory
        self.response = []
        self.qcmd = Queue.Queue()

        self.bus = '{0:02}'.format(bus)

        self.event = threading.Event()
        self.event.set()

        # Zbieranie logów z temperatury i wilgotności
        self.log_file = "measure_log.log"
        self.log_cycle = 60          # Czas co jaki następuje zapis parametrów
        self.capture_values = True

        self.gradients = {
            "heating": 1.0,
            "cooling": 1.0,
            "humidify": 5.0,
            "dehumidify": 5.0,
        }
        self.nominal = {
            "temperature": 23.0,
            "humidity": 36.0,
            "ventilator": 0.0
        }
        self.actual = {
            "temperature": 23.0,
            "humidity": 36.0,
            "ventilator": 0.0
        }
        self.oStart = True
        self.oHumidity = False

    def run(self):
        while self._work_flag and not self.serial.isOpen():
            try:
                self.serial.open()
            except:
                traceback.print_exc(file=sys.stdout)
                return

        while self._work_flag:
            try:
                if self.serial.inWaiting() > 0:
                    data = self.serial.read(self.serial.inWaiting())
                    self.response.append(data)
            except:
                print "Read Error", traceback.format_exc()

    def send(self, cmd):
        print u">", cmd
        try:
            self.response = []
            self.serial.write(cmd)
            time.sleep(self.delay)
            return self.response
        except:
            print "Write Error", traceback.format_exc()
            return None

    def __set_nominal(self, temperature=None, humidity=None, ventilator=None,
                      oStart=None, oHumidity=None):
        if temperature is not None:
            self.nominal["temperature"] = float(temperature)
        if humidity is not None:
            self.nominal["humidity"] = float(humidity)
        if ventilator is not None:
            self.nominal["ventilator"] = float(ventilator)
        if oStart is not None:
            self.oStart = oStart
        if oHumidity is not None:
            self.oHumidity = oHumidity

    def __set_actual_values(self, response):
        tmp = re.findall(r'(.\d{3}\.\d)\s', ''.join(response))
        self.actual["temperature"] = float(tmp[1])
        self.actual["humidity"] = float(tmp[3])
        self.actual["ventilator"] = float(tmp[5])

    def set_gradients(self, heating=None, cooling=None, humidify=None,
                      dehumidify=None):
        if heating is not None:
            self.gradients["heating"] = float(heating)
        if cooling is not None:
            self.gradients["cooling"] = float(cooling)
        if humidify is not None:
            self.gradients["humidify"] = float(humidify)
        if dehumidify is not None:
            self.gradients["dehumidify"] = float(dehumidify)

    def capture_logs(self, log_file=None):
        self.capture_values = True
        if log_file is not None:
            self.log_file = log_file

        def capture():
            while self.capture_values:
                try:
                    self.event.wait()
                    self.event.clear()
                    request = chr(36)+"%sI" % self.bus+chr(13)
                    response = self.send(request)
                    self.__set_actual_values(response)
                    self.event.set()
                    with open(self.log_file, "a") as myfile:
                        myfile.write("%s; %s; %s\n" % (
                            self.actual["temperature"],
                            self.actual["humidity"],
                            datetime.datetime.now()))
                except:
                    print "Capturing Error", traceback.format_exc()
                finally:
                    self.event.set()
                    time.sleep(self.log_cycle-self.delay)
        d = threading.Thread(name='capture_logs', target=capture)
        d.setDaemon(True)
        d.start()

    def stop_capturing_logs(self):
        self.capture_values = False

    def set_values(self, temperature=None, humidity=None, ventilator=None,
                   oStart=None, oHumidity=None):
        self.event.wait()
        self.event.clear()
        self.__set_nominal(temperature, humidity, ventilator, oStart,
                           oHumidity)
        request = chr(36)+"%sE %s %s %s " % (
            self.bus,
            '{0:06.1f}'.format(self.nominal["temperature"]),
            '{0:06.1f}'.format(self.nominal["humidity"]),
            '{0:06.1f}'.format(self.nominal["ventilator"]))
        request += "0000.0 0000.0 0000.0 0000.0 0"
        request += (lambda x: '1' if x else '0')(self.oStart)
        request += (lambda x: '1' if x else '0')(self.oHumidity)
        request += "01010101010101010101010101010"+chr(13)
        self.send(request)
        self.event.set()

    def measure_values(self):
        try:
            self.event.wait()
            self.event.clear()
            request = chr(36)+"%sI" % self.bus+chr(13)
            response = self.send(request)
            self.__set_actual_values(response)
            self.event.set()
        except:
            print "Measure Error", traceback.format_exc()
        finally:
            self.event.set()

    def set_values_gradient(self, temperature=None, humidity=None,
                            ventilator=None, oStart=None, oHumidity=None):

        def work(self):
            self.measure_values()

            actual_temp = self.actual["temperature"]
            if temperature is not None:
                goal_temp = temperature
            else:
                goal_temp = actual_temp
            diff_temp = round(goal_temp - actual_temp, 1)
            grad_dir_temp = (lambda x: self.gradients["heating"] if x > 0 else self.gradients["cooling"])(diff_temp)
            grad_dir_temp = math.copysign(1, diff_temp) * grad_dir_temp
            repeats_temp = round(abs(diff_temp/grad_dir_temp))

            actual_hum = self.actual["humidity"]
            if humidity is not None:
                goal_hum = humidity
            else:
                goal_hum = actual_hum
            diff_hum = round(goal_hum - actual_hum, 1)

            grad_dir_hum = (lambda x: self.gradients["humidify"] if x > 0 else self.gradients["dehumidify"])(diff_hum)
            grad_dir_hum = math.copysign(1, diff_hum) * grad_dir_hum
            repeats_hum = round(abs(diff_hum/grad_dir_hum))

            for i in xrange(int(max([repeats_temp, repeats_hum]))):
                if i < (math.floor(repeats_temp)):
                    actual_temp = actual_temp + grad_dir_temp
                else:
                    actual_temp = goal_temp

                if i < (math.floor(repeats_hum)):
                    actual_hum = actual_hum + grad_dir_hum
                else:
                    actual_hum = goal_hum

                self.set_values(temperature=actual_temp, humidity=actual_hum,
                                oStart=oStart, oHumidity=oHumidity)
                time.sleep(60)

        d = threading.Thread(name='val_gradient', target=work, args=(self,))
        d.setDaemon(True)
        d.start()

    def stop(self):
        self._work_flag = False
        time.sleep(0.1)
        self.serial.close()

if __name__ == "__main__":
    chamber = WKL64(19200, "/dev/ttyUSB0", bus=1)
    chamber.start()
    time.sleep(1)

    chamber.capture_logs(log_file="wkl_temp_test.log")

    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=-20, oStart=True)
    time.sleep(4*60*60)
    chamber.set_values(temperature=50, oStart=True)
    time.sleep(4*60*60)

    chamber.stop_capturing_logs()
    chamber.set_values(temperature=23, oStart=True)
    #chamber.set_values(temperature=-20, oStart=True)
    #time.sleep(60*60)
    #chamber.set_gradients(heating=1, cooling=1, humidify=2, dehumidify=2)
    #chamber.set_values_gradient(temperature=20, oStart=True)

    #chamber.measure_values()
    #print chamber.actual

    # chamber.stop_capturing_logs()
    time.sleep(60*60)
    #chamber.stop_capturing_logs()
    chamber.stop()
