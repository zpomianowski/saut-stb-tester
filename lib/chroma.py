# -*- coding: utf-8 -*-
u'''
Komunikacja RS232
'''
import serial
import Queue
from threading import Thread
import sys
import traceback
import time


class CHROMA(Thread):
    def __init__(self, baudrate=19200, com='/dev/ttyUSB2', timeout=0):
        Thread.__init__(self)
        self.serial = serial.Serial()
        self.serial.baudrate = baudrate
        self.serial.port = com
        serial.timeout = timeout
        self._work_flag = True
        self.response = []
        self.qcmd = Queue.Queue()

        self.skip_read = False

    def run(self):
        while self._work_flag and not self.serial.isOpen():
            try:
                self.serial.open()
            except:
                print traceback.format_exc()
                return
        while self._work_flag:
            try:
                if self._work_flag and not self.qcmd.empty():
                    self.response = ""
                    cmd = self.qcmd.get(False)
                    self.serial.write(cmd+'\n')
            except:
                print "WRITE ERROR", traceback.format_exc()

            try:
                if self._work_flag and (self.serial.inWaiting() > 0):
                    data = self.serial.read(self.serial.inWaiting())  # read(1)
                    self.response.append(data)
            except:
                print "READ ERROR", traceback.format_exc()

    def send(self, cmd):
        #print u">", cmd
        self.qcmd.put(cmd)

    def stop(self):
        self._work_flag = False
        self.serial.close()

    def getresponse(self):
        return self.response


if __name__ == "__main__":
    chroma_w = CHROMA(19200, "/dev/ttyUSB0", 1)
    chroma_w.start()

    time.sleep(1)
    chroma_w.send("OUTPut OFF")
    time.sleep(1)
    chroma_w.send("VOLT:AC 230")
    time.sleep(1)
    chroma_w.send("FREQ 50")
    time.sleep(1)
    chroma_w.send("OUTPut ON")
    time.sleep(1)

    chroma_w.send("FETC:POW:AC?")
    time.sleep(1)

    print float(''.join(chroma_w.getresponse()))

    time.sleep(3)
    chroma_w.send("OUTPut OFF")
    time.sleep(2)
    chroma_w.stop()

    #  FETC:CURR:AC?        - PRĄD
    #  FETC:VOLT:ACDC?      - NAPIĘCIE
    #  FETC:POW:AC:REAC?
    #  FETC:POW:AC?         - MOC
