# -*- coding: utf-8 -*-

import sys
import traceback
import time
import os


class CHROMA66201:
    def __init__(self, device):
        self.device = device
        self.FILE = os.open(device, os.O_RDWR)

    def write(self, command):
        os.write(self.FILE, command)

    def read(self, length=4000):
        try:
            t = os.read(self.FILE, length).replace('\n', '')
            return t.replace('\r',  '')
        except:
            return None

    def getVoltageRMS(self):
        self.write("MEAS:VOLT:RMS?")
        time.sleep(0.2)
        return self.read(100)

    def getCurrentRMS(self):
        self.write("MEAS:CURR:RMS?")
        time.sleep(0.2)
        return self.read(100)

    def getCurrentTHD(self):
        self.write("MEAS:CURR:THD?")
        time.sleep(0.2)
        return self.read(100)

    def getPowerREAL(self):
        self.write("MEAS:POW:REAL?")
        time.sleep(0.2)
        return self.read(100)

    def getFrequency(self):
        self.write("MEAS:FREQ?")
        time.sleep(0.2)
        return self.read(100)

    def getFrequency(self):
        self.write("MEAS:FREQ?")
        time.sleep(0.2)
        return self.read(100)

    def getName(self):
        self.write("*IDN?")
        return self.read(300)

    def sendReset(self):
        self.write("*RST")

if __name__ == "__main__":
    import datetime
    chroma = CHROMA66201("/dev/usbtmc0")
    print chroma.getName()
    time.sleep(1)
    print chroma.getVoltageRMS()
    print chroma.getCurrentRMS()
    print chroma.getCurrentTHD()
    print chroma.getPowerREAL()
    print chroma.getFrequency()
