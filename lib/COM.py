# -*- coding: utf-8 -*-


import serial
import Queue
from threading import Thread
import threading
import sys
import traceback
import time
import os
import datetime
import re
from sys import platform as _platform


class COM(Thread):
    def __init__(self, baudrate=9600, com='/dev/ttyUSB0', timeout=1,
                 logfile=None):
        Thread.__init__(self, name="COM_thread")
        self.serial = serial.Serial()
        self.serial.baudrate = baudrate
        self.serial.port = com
        serial.timeout = timeout

        self.work_flag = True

        self.response = []
        self.logfile = logfile

        self.qcmd = Queue.Queue()
        self.serial.open()

    def run(self):
        while self.work_flag:
            try:
                if self.work_flag and not self.qcmd.empty():
                    self.response = []
                    cmd = self.qcmd.get(False)
                    self.serial.write(cmd+'\n')
            except:
                self.save_log("WRITE ERROR", traceback.format_exc())
                # print "WRITE", traceback.format_exc()

            try:
                if self.work_flag:  # and self.serial.inWaiting() > 0:
                    data = self.serial.read(self.serial.inWaiting())
                    # data = data.replace('\r', '')
                    self.response.append(data)
            except:
                self.save_log("READ ERROR", traceback.format_exc())
                # print "READ", traceback.format_exc()

    def save_log(self, title, msg):
        if self.logfile is not None:
            with open(self.logfile, "a") as myfile:
                myfile.write(
                    "\n\n%s %s\n" % (title, datetime.datetime.now()))
                tmp = '\n'.join(["\t%s" % line for line in msg.split("\n")])
                myfile.write(tmp)

    def get_response(self):
        return ''.join(self.response)

    def send(self, cmd):
        try:
            print u"\tsend: ", cmd
            self.qcmd.put(cmd)
        except:
            self.save_log("SEND ERROR", traceback.format_exc())
            print "SEND", traceback.format_exc()

    def stop(self):
        try:
            self.work_flag = False
            #while not self.qcmd.empty():
            #    self.qcmd.get()
            self.serial.flushOutput()
            self.serial.flushInput()
            self.serial.close()
        except:
            self.save_log("STOP ERROR", traceback.format_exc())
            print "STOP", traceback.format_exc()

if __name__ == "__main__":
    USB_STB = "/dev/ttyUSB0"

    stb = None

    stb = COM(115200, USB_STB, 0, "com_log.log")
    stb.start()

    '''
    stb.send("transponder 0 802000 6875 q256")
    time.sleep(1)
    stb.send("avp_pids_set 0x101 0x100 0x100")   # avp_pids_set 0x204 0x201 0x201
    time.sleep(1)
    stb.send("video_decoder 3")
    time.sleep(1)
    stb.send("audio_decoder 2")
    stb.stop()
    '''

    #print threading.enumerate()name
    #print threading.current_thread()
    #stb.stop()
    # avp_pids_set 0x101 0x100 0x100
