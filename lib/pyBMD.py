# -*- coding: utf-8 -*-
from threading import Thread
from ctypes import *
import numpy as nm

NULL = c_void_p()
lib = CDLL("/home/cp/stb/lib/Decklink/libpyBMD.so")

bmdVideoConnectionSDI = 1 << 0
bmdVideoConnectionHDMI = 1 << 1

### object life control
bmd_New = lib.BMD_New
bmd_New.argtypes = [c_int, c_int, c_int, c_int]
bmd_New.restype = c_void_p

bmd_Delete = lib.BMD_Delete
bmd_Delete.argtypes = [c_void_p]
bmd_Delete.restype = c_void_p

### static methods
bmds_listDisplayModes = lib.BMDS_listDisplayModes
bmds_listDisplayModes.restype = c_void_p

bmds_listSupportedPixelFormats = lib.BMDS_listSupportedPixelFormats

bmds_freeBuffer = lib.BMDS_freeBuffer
bmds_freeBuffer.argtypes = [POINTER(c_char)]
bmds_freeBuffer.restype = c_void_p

### flow control
bmd_switchInputConnection = lib.BMD_switchInputConnection
bmd_switchInputConnection.argtypes = [c_void_p, c_int]
bmd_switchInputConnection.restype = c_int

bmd_startLoop = lib.BMD_startLoop
bmd_startLoop.argtypes = [c_void_p, c_ulong]
bmd_startLoop.restype = c_int

bmd_stopLoop = lib.BMD_stopLoop
bmd_stopLoop.argtypes = [c_void_p]
bmd_stopLoop.restype = c_int

### callbacks setup
bmd_setFrameArrivedCallback = lib.BMD_setFrameArrivedCallback
bmd_setFrameArrivedCallback.argtypes = [c_void_p, c_void_p]
bmd_setFrameArrivedCallback.restype = c_bool

bmd_setStopLoopCallback = lib.BMD_setStopLoopCallback
bmd_setStopLoopCallback.argtypes = [c_void_p, c_void_p]
bmd_setStopLoopCallback.restype = c_void_p

bmd_setStartLoopCallback = lib.BMD_setStartLoopCallback
bmd_setStartLoopCallback.argtypes = [c_void_p, c_void_p]
bmd_setStartLoopCallback.restype = c_void_p

### other
bmd_getFrameFromBuffer = lib.BMD_getFrameFromBuffer
bmd_getFrameFromBuffer.argtypes = [c_void_p]
bmd_getFrameFromBuffer.restype = c_int


### Frames counter
bmd_resetCounters = lib.BMD_resetCounters
bmd_resetCounters.argtypes = [c_void_p]
bmd_resetCounters.restype = c_void_p

bmd_getValidFrames = lib.BMD_getValidFrames
bmd_getValidFrames.argtypes = [c_void_p]
bmd_getValidFrames.restype = c_ulong

bmd_getInValidFrames = lib.BMD_getInValidFrames
bmd_getInValidFrames.argtypes = [c_void_p]
bmd_getInValidFrames.restype = c_ulong

CFRAME = CFUNCTYPE(c_bool, c_long, c_long, c_long, c_int, c_char_p)
CSIGNAL0 = CFUNCTYPE(c_bool)
CSIGNAL2 = CFUNCTYPE(c_bool, c_ulong, c_ulong)


class BMD(Thread):
    #VALID = 0
    #INVALID = 0

    def __init__(self, devid=0, buffersize=10):
        Thread.__init__(self)
        self.handle = bmd_New(devid, 0, 0, buffersize)
        self.callback_frame = None
        self.callback_start = None
        self.callback_stop = None

        self.frames_number = 0

    def delete(self):
        bmd_Delete(self.handle)

    def switchInputConnection(self, input_enum):
        return bmd_switchInputConnection(self.handle, input_enum)

    def startLoop(self, frames_number):
        bmd_startLoop(self.handle, frames_number)

    def startLoopNonBlocking(self, frames_number):
        self.frames_number = frames_number
        self.start()

    def run(self):
        bmd_startLoop(self.handle, self.frames_number)

    def stopLoop(self):
        return bmd_stopLoop(self.handle)

    def getFrameFromBuffer(self, buff_p):
        return bmd_getFrameFromBuffer(self.handle, buff_p)

    def setFrameArrivedCallback(self, func):
        self.callback_frame = CFRAME(func)
        return bmd_setFrameArrivedCallback(self.handle, self.callback_frame)

    def setStartLoopCallback(self, func):
        self.callback_start = CSIGNAL0(func)
        return bmd_setStartLoopCallback(self.handle, self.callback_start)

    def setStopLoopCallback(self, func):
        self.callback_stop = CSIGNAL2(func)
        return bmd_setStopLoopCallback(self.handle, self.callback_stop)

    def resetCounters(self):
        bmd_resetCounters(self.handle)

    def getInvalidFrames(self):
        return bmd_getInValidFrames(self.handle)

    def getValidFrames(self):
        return bmd_getValidFrames(self.handle)

    @staticmethod
    def freeBuffer(buff_p):
        bmds_freeBuffer(buff_p)

    @staticmethod
    def listDisplayModes():
        return bmds_listDisplayModes()

    @staticmethod
    def listSupportedPixelFormats():
        bmds_listSupportedPixelFormats()

### test

import datetime

class VIDEO(Thread):
    def __init__(self, bmd=None, frames=10):
        Thread.__init__(self)
        self.bmd = None
        self.work_flag = True
        self.frames = frames
        self.valid = 0
        self.invalid = 0

    def callback_frame(self, width, height, bytes_in_row, pixel_format, buff_p):
        #print "dev_id: %d, w: %d, h: %d, inrow: %d" % (
        #    BMD_id, width, height, bytes_in_row)
        #import numpy as np
        #buffer = np.frombuffer(buff_p, dtype=np.dtype('b'))
        #BMD.freeBuffer(buffer)
        #print len(buffer)
        return True

    def callback_start(self):
        #print "START!"
        return True

    def callback_stop(self, infalidnb, validnb):
        print infalidnb, validnb, datetime.datetime.now()
        self.valid += int(validnb)
        self.invalid += int(infalidnb)
        #BMD.add(validnb, infalidnb)
        return True

    def run(self):
        self.bmd = BMD(0)

        self.bmd.setStartLoopCallback(self.callback_start)
        self.bmd.setStopLoopCallback(self.callback_stop)
        self.bmd.setFrameArrivedCallback(self.callback_frame)
        while self.work_flag:
            #print "1", datetime.datetime.now()
            self.bmd.startLoop(self.frames)
            #time.sleep(5)
            #print "2", datetime.datetime.now()

    def reset_counter(self):
        self.valid = 0
        self.invalid = 0

    def getInvalidFrames(self):
        return self.bmd.getInvalidFrames()

    def getValidFrames(self):
        return self.bmd.getValidFrames()

    def resetCounters(self):
        self.bmd.resetCounters()

    def get_counter(self):
        return [self.invalid, self.valid]

    def stop(self):
        self.work_flag = False
        time.sleep(1)
        if self.bmd:
            self.bmd.stopLoop()
            self.bmd.delete()

if __name__ == '__main__':
    import sys
    import time
    BMD.listDisplayModes()
    BMD.listSupportedPixelFormats()

    BMD_id = int(sys.argv[1])

    def callback_frame(width, height, bytes_in_row, pixel_format, buff_p):
        #print "dev_id: %d, w: %d, h: %d, inrow: %d" % (
        #    BMD_id, width, height, bytes_in_row)
        import numpy as np
        buffer = np.frombuffer(buff_p, dtype=np.dtype('b'))
        #BMD.freeBuffer(buffer)
        #print len(buffer)
        return True

    def callback_start():
        print "START!"
        return True

    def callback_stop(infalidnb, validnb):
        print infalidnb, validnb
        #BMD.add(validnb, infalidnb)
        return True
    '''
    test1 = BMD(BMD_id)
    test1.setStartLoopCallback(callback_start)
    test1.setStopLoopCallback(callback_stop)
    test1.setFrameArrivedCallback(callback_frame)

    test1.startLoopNonBlocking(0)
    time.sleep(5)
    print test1.getInvalidFrames(), test1.getValidFrames(),  datetime.datetime.now()
    time.sleep(5)
    print test1.getInvalidFrames(), test1.getValidFrames(),  datetime.datetime.now()
    test1.resetCounters()
    time.sleep(5)
    print test1.getInvalidFrames(), test1.getValidFrames(),  datetime.datetime.now()

    test1.stopLoop()
    test1.delete()



    v = VIDEO(frames=0)
    v.start()
    time.sleep(5)
    print v.getInvalidFrames(), v.getValidFrames(), datetime.datetime.now()
    time.sleep(4)
    print v.getInvalidFrames(), v.getValidFrames(), datetime.datetime.now()
    v.resetCounters()
    time.sleep(5)
    print v.getInvalidFrames(), v.getValidFrames(), datetime.datetime.now()

    time.sleep(4)

    v.stop()
    '''
