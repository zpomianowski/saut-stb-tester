# -*- coding: utf-8 -*-
import datetime
import time
import json
from COM import *
from WKL64 import *
from ADBlib import *
from chroma import *

if __name__ == "__main__":
    USB_STB = "/dev/ttyUSB1"
    USB_CHROMA = "/dev/ttyUSB2"
    USB_CHAMBER = "/dev/ttyUSB0"
    filename = "test_logs.log"

    chamber = WKL64(19200, USB_CHAMBER, bus=1)
    chamber.start()
    time.sleep(2)
    chamber.capture_logs()

    chroma = CHROMA(19200, USB_CHROMA, 1)
    chroma.start()
    time.sleep(1)
    chroma.send("VOLT:AC 230")
    chroma.send("FREQ 50")
    chroma.send("OUTPut ON")

    TESTS = [
        {"Toff": 0.025,
         "logfile": "test_A_toff_005_log.log"},
        {"Toff": 0.25,
         "logfile": "test_B_toff_05_log.log"},
        {"Toff": 0.5,
         "logfile": "test_C_toff_1_log.log"},
        {"Toff": 0.75,
         "logfile": "test_D_toff_2_log.log"}
    ]
    TEMPERATURE = [5, 50]
    SUBTEST_TIME = 2*60

    for test in TESTS:

        for temp in TEMPERATURE:
            chamber.set_values(temperature=temp, oStart=True)
            print "\tTEMPERATURE", temp
            time.sleep(10*60)
            START_TIMESTAMP = time.time()
            while True:
                if (time.time() - START_TIMESTAMP) >= SUBTEST_TIME:
                    stb = None
                    TEST_SUCCESS = True
                    Decklink_OK = True
                    try:
                        stb = COM(115200, USB_STB, 3, "com_log_file.log")
                        stb.start()
                        time.sleep(40)
                        # ######################## TATS 0 #####################
                        if not check_tats(stb, test["logfile"]):
                            TEST_SUCCESS = False
                        # ######################## SET DHCP ###################
                        if not setDHCPandPING(stb, test["logfile"]):
                            TEST_SUCCESS = False

                    except:
                        with open("script_error.log", "a") as myfile:
                            myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                            myfile.write("\n%s\n" % traceback.format_exc())
                    finally:

                        if TEST_SUCCESS is True:
                            print "\tTEST - OK"
                            with open(test["logfile"], "a") as myfile:
                                myfile.write("\n\TEST - OK")
                        else:
                            TEST_SUCCESS = False
                            print "\tTEST - ERROR"
                            with open(test["logfile"], "a") as myfile:
                                myfile.write("\n\TEST - ERROR")

                        if stb:
                            stb.stop()
                            stb = None
                        chroma.send("OUTPut OFF")
                        break
                chroma.send("OUTPut OFF")
                time.sleep(test["Toff"])
                chroma.send("OUTPut ON")
                time.sleep(test["Toff"])

    chamber.stop_capturing_logs()
    chamber.set_values(temperature=23, oStart=False)
    time.sleep(1)
    chamber.stop()

    chroma.send("OUTPut OFF")
    time.sleep(1)
    chroma.stop()

    print "END",  datetime.datetime.now()
